# Sistema 

O My Question é um web app para criar e responder questionários. Nele o usuário pode criar um questionário, questões e alternativas de forma pública ou privada e acompanhar a porcentagem das alternativas selecionadas por outros usuários.
As respostas são anónimas e só é possível que cada usuário responda uma única vez um questionário.

## Objetivo

Desenvolver uma aplicação em que os usuários possam desenvolver pesquisas informais por meio de questionários.

## Justificativa

Este projeto tem por finalidade o desenvolvimento de minhas habilidades técnicas.  

## Figma
As telas do projeto podem ser contradas [**aqui**](https://www.figma.com/file/msFA8uibG3tTWg2uGrkjlR/Untitled?node-id=0%3A1).   

## Requisitos

### Requisitos funcionais 

[RF001] - O usuário deve possuir os seguintes atributos: nome, sobrenome, cidade, estado e data de nascimento;

[RF002] - Deve implementar a função de softdelete de usuários;

[RF003] - O questionário deve possuir os seguintes atributos: título, tema, visibilidade(pública ou privada), destinatários, data e hora de início e fim, justificativa e questões. As questões devem possuir os seguintes atributos: título e alternativas;

[RF004] - A descrição do questionário deve ser sempre visível porém, as questões só podem ser visualizadas se iniciado o questionário;

[RF005] - O questionário pode ser público ou privado, caso seja privado deve ser definido o(s) e-mail(s) do(s) destinatário(s);

[RF006] - Após a criação de um questionário só poderá ser alterado o(s) destinatário(s) do mesmo;

[RF007] - Cada usuário só pode realizar uma vez um questionário;

[RF008] - O usuário precisa responder a um questionário para visualizar as alternativas mais selecionadas ou seja, o resultado;

[RF009] - Deve ser possível filtrar um questionário por título ou tema;

[RF010] - Deve ser possível ordenar a listagem do questionário por os mais respondidos e mais recentes;

### Requisitos não funcionais

[RNF001] - Utilizar a linguagem de programação JavaScript na camada de front-end e back-end, banco de dados MySQL, ORM Sequelize, Axios, Biblioteca React, Styled components e Styled System;

[RNF002] - Aplicar o conceito mobile first;

[RFN003] - A home do sistema deve conter uma breve descrição do sistema e os questionários mais votados em ordem decrescente;
